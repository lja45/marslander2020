import numpy as np
import matplotlib.pyplot as plt

E_radius = 64000000 # (6400 km)

# Vectors : Pos [x, y, z] , Vel [x, y, z]

# Calculate the magnitude of an n dimensional vector
def M(r):
    sum = 0
    for n in r:
        sum += n*n
    return np.sqrt(sum)

# Gravity 
def g(r, G = 6.67e-11, m = 5.92e24):
    return [((-G*m)/(M(r)**3)*i) for i in r]

# Euler integration
def euler_method(t_array, p0, v0, f):
    # initialise the t0 values
    position = np.array(p0)
    velocity = np.array(v0)
    # initialise empty lists to record trajectories
    p_list = []
    v_list = []

    for t in t_array:

        # append current state to trajectories
        p_list.append(position) 
        v_list.append(velocity)

        # calculate new position and velocity
        a = f(position)
        position = position + dt * velocity
        velocity = velocity + a
    
    return (p_list, v_list)

def verlet_method(t_array, dt, p0, v0, f):
    # initialise the t0 values
    position = p0
    velocity = v0
    # initialise empty lists to record trajectories
    p_list = [p0, v0*dt]
    v_list = [v0]
    
    for t in t_array:
        # calculate new position and velocity
        position = 2*p_list[len(p_list)-1] - p_list[len(p_list)-2] + dt**2*(f(position))
        # dt - 1 velocity
        velocity = (1/(2*dt)) * (position - p_list[len(p_list)-2])
        
        p_list.append(position)
        v_list.append(velocity)

    p_list.pop()
    p_list.pop()
    v_list.pop()

    return (p_list, v_list)

# simulation time, timestep and time
t_max = 2000
dt = 1
t_array = np.arange(0, t_max, dt)

# Earth at 000, work in xy plane, z effectively nil
p0 = [10000e3,0,0]
v0 = [0,0,0]

p_elist, v_elist = euler_method(t_array, p0, v0, g)
altitude = [M(p) for p in p_elist]

# plot the alt-time graph (/1000 )
plt.figure(1)
plt.subplot(211)
plt.title("Euler Integration")
plt.grid()
plt.plot(t_array, altitude, label='Altitude')
#circle1=plt.Circle((0,0),,color='b')
#plt.gcf().gca().add_artist(circle1)
plt.legend()

plt.show()