# MarsLander2020

CUED Part IA Mars Lander dynamics simulation Project

## Built using the *DDS build system*

Original source files under /base
DDS automatically builds source files under /src and includes files under /main.
All relevant settings are in the package and build manifest.
