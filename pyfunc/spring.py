## SCRATCHPAD for spring

# uncomment the next line if running in a notebook
# %matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

# mass, spring constant, initial position and velocity
m = 1
k = 1

# simulation time, timestep and time
t_max = 100
dt = 0.01
t_array = np.arange(0, t_max, dt)

def f_m(x, k, m):
    return (-k * x / m)

# Euler integration
def euler_method(time, x0, v0):
    # initialise the t0 values
    x = x0
    v = v0
    # initialise empty lists to record trajectories
    x_list = []
    v_list = []

    for t in t_array:

        # append current state to trajectories
        x_list.append(x)
        v_list.append(v)

        # calculate new position and velocity
        a = f_m(x, k, m)
        x = x + dt * v
        v = v + dt * a
    
    return (x_list, v_list)

# Verlet Integration
def verlet_method(t_array, dt, x0, v0):
    # initialise the t0 values
    x = 0
    v = 0
    # initialise empty lists to record trajectories
    x_list = [x0, v0*dt]
    v_list = [v0]
    
    for t in t_array:
        # calculate new position and velocity
        x = 2*x_list[len(x_list)-1] - x_list[len(x_list)-2] + dt**2*(f_m(x,k,m))
        # dt - 1 velocity
        v = (1/(2*dt)) * (x - x_list[len(x_list)-2])
        
        x_list.append(x)
        v_list.append(v)

    x_list.pop()
    x_list.pop()
    v_list.pop()
    
    return (x_list, v_list)

# run the euler method on a()
x_l_euler, v_l_euler = euler_method(t_array, 0, 1)

# run the verlet method on a()
x_l_verlet, v_l_verlet = verlet_method(t_array, dt, 0, 1)

# convert trajectory lists into arrays, so they can be sliced (useful for Assignment 2)
xe_array = np.array(x_l_euler)
ve_array = np.array(v_l_euler)
xv_array = np.array(x_l_verlet)
vv_array = np.array(v_l_verlet)

# plot the position-time graph
plt.figure(1)

plt.subplot(211)
plt.title("Euler Integration")
plt.grid()
plt.plot(t_array, xe_array, label='x - Euler (m)')
plt.plot(t_array, ve_array, label='v - Euler (m/s)')
plt.legend()

plt.subplot(212)
plt.grid()
plt.title("Verlet Integration")
plt.plot(t_array, xv_array, label='x - Verlet (m)')
plt.plot(t_array, vv_array, label='v - Verlet (m/s)')
plt.xlabel('time (s) \n dT = {:.2f}'.format(dt))
plt.legend()
plt.show()
