## SCRATCHPAD for spring 3D

# uncomment the next line if running in a notebook
# %matplotlib inline
import numpy as np
import matplotlib.pyplot as plt

# simulation time, timestep and time
t_max = 10000
dt = 0.1
t_array = np.arange(0, t_max, dt)

# Vectors : Pos [x, y, z] , Vel [x, y, z]

def normal(r):
    sum = 0
    for n in r:
        sum += n*n
    return np.sqrt(sum)

# Euler integration
def euler_method(time, r0, v0, f):
    # initialise the t0 values
    r = r0
    v = v0
    # initialise empty lists to record trajectories
    r_list = [[],[],[]]
    v_list = [[],[],[]]

    for t in t_array:

        # append current state to trajectories
        r_list[0].append(r[0]) # x
        r_list[1].append(r[1]) # y
        r_list[2].append(r[2]) # z

        v_list[0].append(v[0]) # x
        v_list[1].append(v[1]) # x
        v_list[2].append(v[2]) # x

        # calculate new position and velocity
        m = 2
        M = 5.92e24
        a = f(r, m, M)
        r = r + dt * v
        v = v + dt * a
    
    return (r_list, v_list)


def g(r, m, M):
    G = 6.67e-11
    #G = 1
    rns = normal(r)
    return ((-G*m*M)/(rns**3) * r)

# Consider a situatuon in the xy plane

# run the euler method on a()
r0 = np.array([8371*1000,0,0])
v0 = np.array([0,10000,0])
r_l_euler, v_l_euler = euler_method(t_array, r0, v0, g)
# convert trajectory lists into arrays, so they can be sliced (useful for Assignment 2)
r_array = np.array(r_l_euler)
v_array = np.array(v_l_euler)


# plot the position-time graph
plt.figure(1)
plt.subplot(211)
plt.title("Euler Integration")
plt.grid()
plt.plot(r_array[0]/1000, r_array[1]/1000, label='r (x,y) - Euler (m)')

circle1=plt.Circle((0,0),6400,color='b')
plt.gcf().gca().add_artist(circle1)
#plt.plot(t_array, ve_array, label='v - Euler (m/s)')
plt.legend()

plt.subplot(212)
plt.grid()
vt = np.transpose(v_array)
vnorm  = [normal(v) for v in vt]
plt.plot(vnorm, label='v (t) - Euler (m)')
#plt.plot(t_array, ve_array, label='v - Euler (m/s)')
plt.legend()

plt.show()
