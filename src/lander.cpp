// Mars lander simulator
// Version 1.11
// Mechanical simulation functions
// Gabor Csanyi and Andrew Gee, August 2019

// Permission is hereby granted, free of charge, to any person obtaining
// a copy of this software and associated documentation, to make use of it
// for non-commercial purposes, provided that (a) its original authorship
// is acknowledged and (b) no modified versions of the source code are
// published. Restriction (b) is designed to protect the integrity of the
// exercise for future generations of students. The authors would be happy
// to receive any suggested modifications by private correspondence to
// ahg@eng.cam.ac.uk and gc121@eng.cam.ac.uk.

#include "lander.h"
#include <algorithm>

// Autopilot Control
// 0.019, 1 (scen 5  suicide burn)
static double Kh = 0.019;
static double Kp = 0.05;
static double delta_offset = 1;



// Dynamics Control
#define USE_VERLET
//#define USE_EULER
// previous cases
static vector3d r_m2;
static vector3d r_m1;
static vector3d v_m1;
// Gravity
vector3d fG(vector3d r){
  double mass = fuel*FUEL_CAPACITY*FUEL_DENSITY + UNLOADED_LANDER_MASS;
  vector3d ret = ((GRAVITY*MARS_MASS*mass)/(pow(r.abs(), 3))) * -r;
  return ret;
}
// Drag 
vector3d fD(vector3d r, vector3d v){
  vector3d ret;
  double roe = atmospheric_density(r);
  // Body Drag
  // Take circular base as projected area
  ret -= 0.5*roe*DRAG_COEF_LANDER*LANDER_SIZE*LANDER_SIZE*3.1416*velocity.abs()*velocity;
  // Add drag due to parachute
  // take sum of 5 squares side (2xlandersize)
  if (parachute_status == DEPLOYED){
    ret -= 0.5*DRAG_COEF_CHUTE*roe*5.0*pow(2.0*LANDER_SIZE, 2)*velocity.abs()*velocity;
  }
  return ret;
}
// Thrust
vector3d fT(){
  return thrust_wrt_world();
}

float k_V(float s = 1){
    float Kp = 0.01;
    float Kd = 0.01;
    return Kp + Kd*s;
}

void autopilot (void)
  // Autopilot to adjust the engine throttle, parachute and attitude control
{
  int target_altitude = 500;
  
  // Basic Autopilot ~ Scen 1 & 5 (Simple Descent)

  // Enable geocentric attitude stabilisation
  stabilized_attitude = true;
  // offset force calculation (Assuming only force due to gravity)
  delta_offset = (fG(position).abs() / MAX_THRUST);
  // error calculation
  double alt = position.abs() - MARS_RADIUS;
  double e = -(0.5 + Kh*alt + velocity * (position/position.abs()));
  // apply the throttle 
  // std::cout << e << "\n";
  // actual DR, target DR, alt
  // std::cout << velocity * (position/position.abs()) << "," << -(0.5 + Kh*alt) << "," << alt << "\n";
  //throttle = clamp(Kp*e + delta_offset, (double)0, (double)1);

  // EP1:
  //throttle = 747.1/MAX_THRUST;

  // EP2:
  //std::cout << "T/A/E: " << target_altitude << "," << - alt << "," << target_altitude-alt<< "\n";
  throttle = k_V(target_altitude-alt) + 747.1/MAX_THRUST;

  std::cout << "T/E/O: " << target_altitude << "," << target_altitude - alt << "," << k_V(target_altitude-alt) << "," << throttle << "\n";
}

void numerical_dynamics (void)
  // This is the function that performs the numerical integration to update the
  // lander's pose. The time step is delta_t (global variable).
{
  if (simulation_time == 0){
    // t0 initial conditions
    r_m2 = position;
    r_m1 = position + velocity*delta_t;
    v_m1 = velocity;
  }
  else{
    double mass = fuel*FUEL_CAPACITY*FUEL_DENSITY + UNLOADED_LANDER_MASS;
    vector3d A = (fG(position) + fD(position, velocity) + fT())/mass;

#ifdef USE_VERLET    
    // Verlet Integrator: (Acceleration function A)
    position = 2*r_m1 - r_m2 + pow(delta_t, 2) * (A);
    velocity = (1/(2*delta_t)) * (position - r_m2);
#elif defined(USE_EULER)
    // Euler Integrator: (Acceleration function A)
    position = r_m1 + delta_t*v_m1;
    velocity = v_m1 + delta_t*(A);
#else
  #error "No Integration Method Specified"
#endif
    r_m2 = r_m1;
    r_m1 = position;
    v_m1 = velocity;
  }

  // Here we can apply an autopilot to adjust the thrust, parachute and attitude
  if (autopilot_enabled) autopilot();

  // Here we can apply 3-axis stabilization to ensure the base is always pointing downwards
  if (stabilized_attitude) attitude_stabilization();
}

void initialize_simulation (void)
  // Lander pose initialization - selects one of 10 possible scenarios
{
  // The parameters to set are:
  // position - in Cartesian planetary coordinate system (m)
  // velocity - in Cartesian planetary coordinate system (m/s)
  // orientation - in lander coordinate system (xyz Euler angles, degrees)
  // delta_t - the simulation time step
  // boolean state variables - parachute_status, stabilized_attitude, autopilot_enabled
  // scenario_description - a descriptive string for the help screen

  scenario_description[0] = "circular orbit";
  scenario_description[1] = "descent from 10km";
  scenario_description[2] = "elliptical orbit, thrust changes orbital plane";
  scenario_description[3] = "polar launch at escape velocity (but drag prevents escape)";
  scenario_description[4] = "elliptical orbit that clips the atmosphere and decays";
  scenario_description[5] = "descent from 200km";
  scenario_description[6] = "EP1: 500";
  scenario_description[7] = "EP1: 510";
  scenario_description[8] = "EP1: 700";
  scenario_description[9] = "";

  switch (scenario) {

  case 0:
    // a circular equatorial orbit
    position = vector3d(1.2*MARS_RADIUS, 0.0, 0.0);
    velocity = vector3d(0.0, -3247.087385863725, 0.0);
    orientation = vector3d(0.0, 90.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
    autopilot_enabled = false;
    break;

  case 1:
    // a descent from rest at 10km altitude
    position = vector3d(0.0, -(MARS_RADIUS + 10000.0), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
    autopilot_enabled = false;
    break;

  case 2:
    // an elliptical polar orbit
    position = vector3d(0.0, 0.0, 1.2*MARS_RADIUS);
    velocity = vector3d(3500.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
    autopilot_enabled = false;
    break;

  case 3:
    // polar surface launch at escape velocity (but drag prevents escape)
    position = vector3d(0.0, 0.0, MARS_RADIUS + LANDER_SIZE/2.0);
    velocity = vector3d(0.0, 0.0, 5027.0);
    orientation = vector3d(0.0, 0.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
    autopilot_enabled = false;
    break;

  case 4:
    // an elliptical orbit that clips the atmosphere each time round, losing energy
    position = vector3d(0.0, 0.0, MARS_RADIUS + 100000.0);
    velocity = vector3d(4000.0, 0.0, 0.0);
    orientation = vector3d(0.0, 90.0, 0.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = false;
    autopilot_enabled = false;
    break;

  case 5:
    // a descent from rest at the edge of the exosphere
    position = vector3d(0.0, -(MARS_RADIUS + EXOSPHERE), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.1;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
    autopilot_enabled = false;
    break;

  case 6:
    // 2P6 EP1 500
    position = vector3d(0.0, -(MARS_RADIUS + 500.0), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.01;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
    autopilot_enabled = true;
    break;
    break;

  case 7:
    // 2P6 EP1 510
    position = vector3d(0.0, -(MARS_RADIUS + 510.0), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.01;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
    autopilot_enabled = true;
    break;

  case 8:
    // 2P6 EP1 700
    position = vector3d(0.0, -(MARS_RADIUS + 700.0), 0.0);
    velocity = vector3d(0.0, 0.0, 0.0);
    orientation = vector3d(0.0, 0.0, 90.0);
    delta_t = 0.01;
    parachute_status = NOT_DEPLOYED;
    stabilized_attitude = true;
    autopilot_enabled = true;
    break;

  case 9:
    break;

  }
}
